/**
* by jingma 2024-05-24
*/
package cn.benma666.kettle.domain;

import cn.benma666.domain.BasicBean;
import lombok.*;
import org.beetl.sql.annotation.entity.AssignID;
import org.beetl.sql.annotation.entity.Table;

/**
 * 
 */
@Table(name = "R_TRANSFORMATION")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RTransformation extends BasicBean{

	/**
	 * 
	 */
	private String createdDate;
	/**
	 * 
	 */
	private String createdUser;
	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private String diffMaxdate;
	/**
	 * 
	 */
	private String extendedDescription;
	/**
	 * 
	 */
	private String fieldNameMaxdate;
	/**
	 * 
	 */
	private Integer idDatabaseLog;
	/**
	 * 
	 */
	private Integer idDatabaseMaxdate;
	/**
	 * 
	 */
	private Integer idDirectory;
	/**
	 * 
	 */
	private Integer idStepInput;
	/**
	 * 
	 */
	private Integer idStepOutput;
	/**
	 * 
	 */
	private Integer idStepRead;
	/**
	 * 
	 */
	private Integer idStepUpdate;
	/**
	 * 
	 */
	private Integer idStepWrite;
	/**
	 * 
	 */
	@AssignID("idGenerator")
	private Integer idTransformation;
	/**
	 * 
	 */
	private String modifiedDate;
	/**
	 * 
	 */
	private String modifiedUser;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private String offsetMaxdate;
	/**
	 * 
	 */
	private Integer sizeRowset;
	/**
	 * 
	 */
	private String tableNameLog;
	/**
	 * 
	 */
	private String tableNameMaxdate;
	/**
	 * 
	 */
	private Integer transStatus;
	/**
	 * 
	 */
	private String transVersion;
	/**
	 * 
	 */
	private String useBatchid;
	/**
	 * 
	 */
	private String useLogfield;
}
