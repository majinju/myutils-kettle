/**
* by jingma 2024-05-24
*/
package cn.benma666.kettle.domain;

import cn.benma666.domain.BasicBean;
import cn.benma666.sjzt.SjsjEntity;
import lombok.*;
import org.beetl.sql.annotation.entity.AssignID;
import org.beetl.sql.annotation.entity.Table;

/**
 * kettle-扩展-作业日志
 */
@Table(name = "kettle_kz_log")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@SjsjEntity
public class KettleKzLog extends BasicBean{

	/**
	 * 创建人代码@SYS_COMMON_USER;
	 */
	private String cjrdm;
	/**
	 * 创建人单位代码@SYS_COMMON_ORG;
	 */
	private String cjrdwdm;
	/**
	 * 创建人单位名称;
	 */
	private String cjrdwmc;
	/**
	 * 创建人姓名;
	 */
	private String cjrxm;
	/**
	 * 创建时间;
	 */
	private String cjsj;
	/**
	 * 调度节点@KETTLE_GLPT_ZYGL_DDJD
	 */
	private String ddjd;
	/**
	 * 更新时间;
	 */
	private String gxsj;
	/**
	 * 主键;
	 */
	@AssignID("idGenerator")
	private String id;
	/**
	 * 作业主键
	 */
	private Integer idJob;
	/**
	 * 结束时间
	 */
	private String jssj;
	/**
	 * 开始时间
	 */
	@AssignID("idGenerator")
	private String kssj;
	/**
	 * 扩展信息;JSON格式
	 */
	private String kzxx;
	/**
	 * 作业名称
	 */
	private String name;
	/**
	 * 排序;
	 */
	private Integer px;
	/**
	 * 日志文件
	 */
	private String rzwj;
	/**
	 * 运行结果
	 */
	private String yxjg;
	/**
	 * 有效性@SYS_COMMON_LJPD;
	 */
	private String yxx;
}
