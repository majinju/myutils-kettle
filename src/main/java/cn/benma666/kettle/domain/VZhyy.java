/**
* by jingma 2024-05-24
*/
package cn.benma666.kettle.domain;

import cn.benma666.domain.BasicBean;
import lombok.*;
import org.beetl.sql.annotation.entity.Table;

/**
 * VIEW
 */
@Table(name = "v_zhyy")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VZhyy extends BasicBean{

	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private Integer idJob;
	/**
	 * 
	 */
	private Integer idJobentry;
	/**
	 * 
	 */
	private String jeName;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private String zhlj;
	/**
	 * 
	 */
	private String zhmc;
}
