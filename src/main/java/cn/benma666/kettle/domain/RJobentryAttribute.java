/**
* by jingma 2024-05-24
*/
package cn.benma666.kettle.domain;

import cn.benma666.domain.BasicBean;
import lombok.*;
import org.beetl.sql.annotation.entity.Table;

/**
 * 
 */
@Table(name = "r_jobentry_attribute")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RJobentryAttribute extends BasicBean{

	/**
	 * 
	 */
	private String code;
	/**
	 * 
	 */
	private Integer idJob;
	/**
	 * 
	 */
	private Integer idJobentry;
	/**
	 * 
	 */
	private Long idJobentryAttribute;
	/**
	 * 
	 */
	private Integer nr;
	/**
	 * 
	 */
	private Double valueNum;
	/**
	 * 
	 */
	private String valueStr;
}
