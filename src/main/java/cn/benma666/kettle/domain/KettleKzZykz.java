/**
* by jingma 2023-05-19
*/
package cn.benma666.kettle.domain;

import cn.benma666.domain.BasicBean;
import cn.benma666.sjzt.SjsjEntity;
import lombok.*;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * kettle-扩展-作业扩展
 */
@Table(name = "kettle_kz_zykz")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@SjsjEntity
public class KettleKzZykz extends BasicBean {

	/**
	 * 创建时间;
	 */
	@Column("cjsj")
	private String cjsj;
	/**
	 * 更新时间;
	 */
	@Column("gxsj")
	private String gxsj;
	/**
	 * 有效性@SYS_COMMON_LJPD;
	 */
	@Column("yxx")
	private String yxx;
	/**
	 * 排序;
	 */
	@Column("px")
	private Integer px;
	/**
	 * 扩展信息;JSON格式
	 */
	@Column("kzxx")
	private String kzxx;
	/**
	 * 创建人姓名;
	 */
	@Column("cjrxm")
	private String cjrxm;
	/**
	 * 创建人代码@SYS_COMMON_USER;
	 */
	@Column("cjrdm")
	private String cjrdm;
	/**
	 * 创建人单位名称;
	 */
	@Column("cjrdwmc")
	private String cjrdwmc;
	/**
	 * 创建人单位代码@SYS_COMMON_ORG;
	 */
	@Column("cjrdwdm")
	private String cjrdwdm;
	/**
	 * 主键;
	 */
	@Id
	@GeneratedValue(generator="idGenerator")
	@Column("id")
	private String id;
	/**
	 * 作业主键
	 */
	@Column("id_job")
	private Integer idJob;
	/**
	 * 作业名称
	 */
	@Column("name")
	private String name;
	/**
	 * 运行状态
	 */
	@Column("yxzt")
	private String yxzt;
	/**
	 * 最后更新时间
	 */
	@Column("zhgxsj")
	private String zhgxsj;
	/**
	 * 自动重启次数
	 */
	@Column("zdcqcs")
	private Integer zdcqcs;
	/**
	 * 调度节点
	 */
	@Column("ddjd")
	private String ddjd;
	/**
	 * 定时描述
	 */
	@Column("dsms")
	private String dsms;
	/**
	 * 日志级别
	 */
	@Column("rzjb")
	private String rzjb;
	/**
	 * 作业类型@OTHER_KETTLE_ZYLX
	 */
	@Column("zylx")
	private String zylx;
	/**
	 * 工作路径
	 */
	@Column("gzlj")
	private String gzlj;
	/**
	 * shell脚本
	 */
	@Column("shell")
	private String shell;
	/**
	 * 数据载体
	 */
	@Column("sjzt")
	private String sjzt;
	/**
	 * sql脚本
	 */
	@Column("sql")
	private String sql;
	/**
	 * js脚本
	 */
	@Column("js")
	private String js;
	/**
	 * KM类名
	 */
	@Column("kmlm")
	private String kmlm;
	/**
	 * KM配置
	 */
	@Column("kmpz")
	private String kmpz;
	/**
	 * 来源对象
	 */
	@Column("lydx")
	private String lydx;
	/**
	 * 目标对象
	 */
	@Column("mbdx")
	private String mbdx;
	/**
	 * 流转模板
	 */
	@Column("lzmb")
	private String lzmb;
	/**
	 * 更多配置
	 */
	@Column("gdpz")
	private String gdpz;
	/**
	 * 输入组件
	 */
	@Column("srzj")
	private String srzj;
	/**
	 * 输出组件
	 */
	@Column("sczj")
	private String sczj;
	/**
	 * 备注
	 */
	@Column("bz")
	private String bz;
	/**
	 * 资源库
	 */
	private String zyk;
}
