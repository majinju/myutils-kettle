/**
* by jingma 2023-05-19
*/
package cn.benma666.kettle.domain;

import cn.benma666.domain.BasicBean;
import cn.benma666.sjzt.SjsjEntity;
import lombok.*;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * kettle-扩展-作业预警
 */
@Table(name = "kettle_kz_log")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@SjsjEntity
public class KettleKzZyyj extends BasicBean {

	/**
	 * 创建时间;
	 */
	@Column("cjsj")
	private String cjsj;
	/**
	 * 更新时间;
	 */
	@Column("gxsj")
	private String gxsj;
	/**
	 * 有效性@SYS_COMMON_LJPD;
	 */
	@Column("yxx")
	private String yxx;
	/**
	 * 排序;
	 */
	@Column("px")
	private BigDecimal px;
	/**
	 * 扩展信息;JSON格式
	 */
	@Column("kzxx")
	private String kzxx;
	/**
	 * 创建人姓名;
	 */
	@Column("cjrxm")
	private String cjrxm;
	/**
	 * 创建人代码@SYS_COMMON_USER;
	 */
	@Column("cjrdm")
	private String cjrdm;
	/**
	 * 创建人单位名称;
	 */
	@Column("cjrdwmc")
	private String cjrdwmc;
	/**
	 * 创建人单位代码@SYS_COMMON_ORG;
	 */
	@Column("cjrdwdm")
	private String cjrdwdm;
	/**
	 * 主键;
	 */
	@Id
	@GeneratedValue(generator="idGenerator")
	@Column("id")
	private String id;
	/**
	 * 作业主键
	 */
	@Column("id_job")
	private BigDecimal idJob;
	/**
	 * 作业名称
	 */
	@Column("name")
	private String name;
	/**
	 * 日志文件
	 */
	@Column("rzwj")
	private String rzwj;
	/**
	 * 消息
	 */
	@Column("xx")
	private String xx;
	/**
	 * 日志级别
	 */
	@Column("rzjb")
	private String rzjb;
	/**
	 * 属主
	 */
	@Column("sz")
	private String sz;
	/**
	 * 日志通道
	 */
	@Column("rztd")
	private String rztd;
	/**
	 * 是否错误
	 */
	@Column("sfcw")
	private String sfcw;
}
