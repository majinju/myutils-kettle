/**
* by jingma 2023-05-19
*/
package cn.benma666.kettle.domain;

import cn.benma666.domain.BasicBean;
import cn.benma666.kettle.loglistener.FileLoggingEventListener;
import cn.benma666.myutils.DateUtil;
import cn.benma666.myutils.StringUtil;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.*;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;
import org.pentaho.di.job.Job;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.File;

/**
 * 作业视图
 */
@Table(name = "v_job")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VJob extends BasicBean {
	public VJob(String zyk, Integer id_job, String yxzt,String ddjd) {
		this(zyk,id_job,yxzt);
		this.ddjd=ddjd;
	}
	public VJob(String zyk, Integer id_job, String yxzt) {
		this.zyk = zyk;
		this.idJob = id_job;
		this.yxzt = yxzt;
		this.zhgxsj = DateUtil.getGabDate();
	}
	public VJob(String zyk, Integer id_job) {
		this.idJob = id_job;
		this.zyk = zyk;
	}

	/**
	 *
	 */
	@Column("ID_DIRECTORY")
	private Long idDirectory;
	/**
	 *
	 */
	@Column("DESCRIPTION")
	private String description;
	/**
	 *
	 */
	@Column("EXTENDED_DESCRIPTION")
	private String extendedDescription;
	/**
	 *
	 */
	@Column("JOB_VERSION")
	private String jobVersion;
	/**
	 *
	 */
	@Column("JOB_STATUS")
	private Integer jobStatus;
	/**
	 *
	 */
	@Column("ID_DATABASE_LOG")
	private Integer idDatabaseLog;
	/**
	 *
	 */
	@Column("TABLE_NAME_LOG")
	private String tableNameLog;
	/**
	 *
	 */
	@Column("CREATED_USER")
	private String createdUser;
	/**
	 *
	 */
	@Column("CREATED_DATE")
	private String createdDate;
	/**
	 *
	 */
	@Column("MODIFIED_USER")
	private String modifiedUser;
	/**
	 *
	 */
	@Column("MODIFIED_DATE")
	private String modifiedDate;
	/**
	 *
	 */
	@Column("USE_BATCH_ID")
	private String useBatchId;
	/**
	 *
	 */
	@Column("PASS_BATCH_ID")
	private String passBatchId;
	/**
	 *
	 */
	@Column("USE_LOGFIELD")
	private String useLogfield;
	/**
	 *
	 */
	@Column("SHARED_FILE")
	private String sharedFile;

	/**
	 * 创建时间;
	 */
	@Column("cjsj")
	private String cjsj;
	/**
	 * 更新时间;
	 */
	@Column("gxsj")
	private String gxsj;
	/**
	 * 有效性@SYS_COMMON_LJPD;
	 */
	@Column("yxx")
	private String yxx;
	/**
	 * 排序;
	 */
	@Column("px")
	private Integer px;
	/**
	 * 扩展信息;JSON格式
	 */
	@Column("kzxx")
	private String kzxx;
	/**
	 * 创建人姓名;
	 */
	@Column("cjrxm")
	private String cjrxm;
	/**
	 * 创建人代码@SYS_COMMON_USER;
	 */
	@Column("cjrdm")
	private String cjrdm;
	/**
	 * 创建人单位名称;
	 */
	@Column("cjrdwmc")
	private String cjrdwmc;
	/**
	 * 创建人单位代码@SYS_COMMON_ORG;
	 */
	@Column("cjrdwdm")
	private String cjrdwdm;
	/**
	 * 主键;
	 */
	@Id
	@GeneratedValue(generator="idGenerator")
	@Column("id")
	private String id;
	/**
	 * 作业主键
	 */
	@Column("id_job")
	private Integer idJob;
	/**
	 * 作业名称
	 */
	@Column("name")
	private String name;
	/**
	 * 运行状态
	 */
	@Column("yxzt")
	private String yxzt;
	/**
	 * 最后更新时间
	 */
	@Column("zhgxsj")
	private String zhgxsj;
	/**
	 * 自动重启次数
	 */
	@Column("zdcqcs")
	private Integer zdcqcs;
	/**
	 * 调度节点
	 */
	@Column("ddjd")
	private String ddjd;
	/**
	 * 定时描述
	 */
	@Column("dsms")
	private String dsms;
	/**
	 * 日志级别
	 */
	@Column("rzjb")
	private String rzjb;
	/**
	 * 作业类型@OTHER_KETTLE_ZYLX
	 */
	@Column("zylx")
	private String zylx;
	/**
	 * 工作路径
	 */
	@Column("gzlj")
	private String gzlj;
	/**
	 * shell脚本
	 */
	@Column("shell")
	private String shell;
	/**
	 * 数据载体
	 */
	@Column("sjzt")
	private String sjzt;
	/**
	 * sql脚本
	 */
	@Column("sql")
	private String sql;
	/**
	 * js脚本
	 */
	@Column("js")
	private String js;
	/**
	 * KM类名
	 */
	@Column("kmlm")
	private String kmlm;
	/**
	 * KM配置
	 */
	@Column("kmpz")
	private String kmpz;
	/**
	 * 来源对象
	 */
	@Column("lydx")
	private String lydx;
	/**
	 * 目标对象
	 */
	@Column("mbdx")
	private String mbdx;
	/**
	 * 流转模板
	 */
	@Column("lzmb")
	private String lzmb;
	/**
	 * 更多配置
	 */
	@Column("gdpz")
	private String gdpz;
	/**
	 * 输入组件
	 */
	@Column("srzj")
	private String srzj;
	/**
	 * 输出组件
	 */
	@Column("sczj")
	private String sczj;
	/**
	 * 目录名称
	 */
	@Transient
	private String directoryName;
	/**
	 * 重新生成
	 */
	@Transient
	private Boolean cxsc;
	/**
	 * 目录名称
	 */
	@Transient
	private String zdys;
	/**
	 * 已重启次数
	 */
	@Transient
	private int ycqcs;
	/**
	 * 备注
	 */
	@Column("bz")
	private String bz;
	/**
	 * 资源库
	 */
	@Transient
	private String zyk;
	/**
	 * 作业对象
	 */
	@JSONField(serialize=false)
	@Transient
	private Job job;
	/**
	 * 日志文件
	 */
	@Transient
	private File rzwj;
	/**
	 * 基础日志主键
	 */
	@Transient
	private String jcrzzj;
	/**
	 * 作业开始时间
	 */
	@Transient
	private String zykssj;
	/**
	 * 复制作业时的目标作业
	 */
	@Transient
	private String mbzy;
	/**
	 * 文件日志监听器
	 */
	@Transient
	private FileLoggingEventListener flel;
	/**
	 * 是否加载作业图
	 */
	@Transient
	private boolean zyt;

	/**
	 * 缓存key
	 */
	public String getKey(){
		return getZyk()+"&"+ getIdJob();
	}
	public Job getJob(){
		return StringUtil.requireNonNull(job,"作业实体不存在");
	}
	public String getZyk(){
		return StringUtil.requireNonNull(zyk,"资源库不能为空");
	}

	public Integer getZdcqcs() {
		return StringUtil.valByDef(zdcqcs,0);
	}
}
