/**
* by jingma 2024-05-24
*/
package cn.benma666.kettle.domain;

import cn.benma666.domain.BasicBean;
import lombok.*;
import org.beetl.sql.annotation.entity.AssignID;
import org.beetl.sql.annotation.entity.Table;

/**
 * 
 */
@Table(name = "r_database")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RDatabase extends BasicBean{

	/**
	 * 
	 */
	private String dataTbs;
	/**
	 * 
	 */
	private String databaseName;
	/**
	 * 
	 */
	private String hostName;
	/**
	 * 
	 */
	@AssignID("idGenerator")
	private Integer idDatabase;
	/**
	 * 
	 */
	private Integer idDatabaseContype;
	/**
	 * 
	 */
	private Integer idDatabaseType;
	/**
	 * 
	 */
	private String indexTbs;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private String password;
	/**
	 * 
	 */
	private Integer port;
	/**
	 * 
	 */
	private String servername;
	/**
	 * 
	 */
	private String username;
}
