/**
* by jingma 2024-05-24
*/
package cn.benma666.kettle.domain;

import cn.benma666.domain.BasicBean;
import lombok.*;
import org.beetl.sql.annotation.entity.Table;

/**
 * VIEW
 */
@Table(name = "v_zycs")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VZycs extends BasicBean{

	/**
	 * 更新时间;
	 */
	private String gxsj;
	/**
	 * 
	 */
	private Integer id;
	/**
	 * 
	 */
	private Integer idJob;
	/**
	 * 
	 */
	private String key;
	/**
	 * 
	 */
	private String name;
	/**
	 * 
	 */
	private String paramDefault;
	/**
	 * 值
	 */
	private String value;
	/**
	 * 有效性@SYS_COMMON_LJPD;
	 */
	private String yxx;
}
