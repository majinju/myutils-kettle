/**
* by jingma 2024-05-24
*/
package cn.benma666.kettle.domain;

import cn.benma666.domain.BasicBean;
import lombok.*;
import org.beetl.sql.annotation.entity.Table;

/**
 * 
 */
@Table(name = "r_jobentry")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RJobentry extends BasicBean{

	/**
	 * 
	 */
	private String description;
	/**
	 * 
	 */
	private Integer idJob;
	/**
	 * 
	 */
	private Integer idJobentry;
	/**
	 * 
	 */
	private Integer idJobentryType;
	/**
	 * 
	 */
	private String name;
}
