/**
* Project Name:myservice
* Date:2018年12月16日
* Copyright (c) 2018, jingma All Rights Reserved.
*/

package cn.benma666.kettle.ljq;

import cn.benma666.exception.MyException;
import cn.benma666.iframe.MyParams;
import cn.benma666.iframe.Result;
import cn.benma666.kettle.mytuils.Kettle;
import cn.benma666.myutils.FileUtil;
import cn.benma666.myutils.StringUtil;
import com.alibaba.fastjson.JSONObject;

import java.awt.image.BufferedImage;
import java.util.Base64;
import java.util.List;

/**
 * 转换拦截器 <br/>
 * date: 2018年12月16日 <br/>
 * @author jingma
 * @version 0.1
 */
public class TransLjq extends KettleLjq {
    /**
    * 转换id
    */
    public static final String ID_TRANSFORMATION = "idTransformation";

    @Override
    public Result plsc(MyParams myParams) {
        //转换列表
        myParams.set("$.page.pageSize",1000);
        myParams.set("$.page.totalRequired",false);
        List<JSONObject> transList = select(myParams).getPageList(JSONObject.class);
        //批量删除转换
        int flag = 0;
        for(JSONObject trans : transList){
            try {
                Kettle.use(getSjdx().getDxzt()).delTrans(trans.getLongValue(ID_TRANSFORMATION));
            } catch (Exception e) {
                flag++;
                log.error("删除job失败:"+trans, e);
            }
        }
        if(flag==0){
            return success("删除转换成功："+transList.size());
        }else{
            return failed("删除成功转换数："+(transList.size()-flag)+"，失败转换数："+flag+"，请查看系统日志分析原因！");
        }
    }

    /**
     * 获取目录
     */
    public Result ml(MyParams myParams) {
        myParams.set("$.page.pageSize",1);
        myParams.set("$.page.totalRequired",false);
        List<JSONObject> jobs = select(myParams).getPageList(JSONObject.class);
        JSONObject transJson = jobs.get(0);
        //转换目录
        try {
            String dir = Kettle.use(getSjdx().getDxzt())
                    .getDirectory(Integer.parseInt(transJson.getString("idDirectory")));
            return success("【"+transJson.getString("name")+"】转换目录："+dir);
        } catch (Exception e) {
            log.error("获取转换目录失败:"+transJson, e);
            return failed("获取转换目录失败，请查看系统日志分析原因:"+e.getMessage());
        }
    }
    @Override
    public Result jcxx(MyParams myParams) {
        myParams = (MyParams) super.jcxx(myParams).getData();
        if(StringUtil.isNotBlank(myParams.getString("$.yobj."+ID_TRANSFORMATION))){
            if(!myParams.containsKey(KEY_OBJ)){
                //没有获取到数据库记录
                return success("获取基础信息成功",myParams);
            }
            if(myParams.getBooleanValue("$.yobj.zht")){
                //作业图场景
                try {
                    BufferedImage image = Kettle.generateTransformationImage(
                            Kettle.use(getSjdx().getDxzt()).loadTrans(myParams
                            .getLongValue("$.yobj."+ID_TRANSFORMATION)));
                    myParams.set("$.obj.zht", Base64.getEncoder().encodeToString(FileUtil.toBytes(image)));
                    return success("获取基础信息成功",myParams);
                } catch (Exception e) {
                    throw new MyException("获取转换图失败",e);
                }
            }
        }
        return success("获取基础信息成功",myParams);
    }
    /**
     * 导入转换
     */
    public Result drzh(MyParams myParams) {
        return failed("暂未实现");
    }

}
