/**
* Project Name:myservice
* Date:2018年12月16日
* Copyright (c) 2018, jingma All Rights Reserved.
*/

package cn.benma666.kettle.ljq;

import cn.benma666.iframe.MyParams;
import cn.benma666.iframe.Result;
import cn.benma666.kettle.domain.VJob;
import cn.benma666.kettle.mytuils.TimingUtil;
import cn.benma666.kettle.vo.ZyglDssz;
import org.pentaho.di.core.exception.KettleException;

/**
 * 作业定时设置拦截器 <br/>
 * date: 2018年12月16日 <br/>
 * @author jingma
 * @version 0.1
 */
public class JobDsszLjq extends KettleLjq {
    @Override
    public Result jcxx(MyParams myParams) {
        ZyglDssz zyglDssz = myParams.yobj(ZyglDssz.class);
        myParams.cllxkz().set("$."+myParams.sys().getCllx()+".putobj",false);
        myParams = (MyParams) super.jcxx(myParams).getData();
        //取出对应作业的定时信息
        ZyglDssz timing = TimingUtil.getTimingByJobId(new VJob(zyglDssz.getZyk(),zyglDssz.getIdJob()));
        timing.setIdJob(zyglDssz.getIdJob());
        timing.setZyk(zyglDssz.getZyk());
        myParams.put(KEY_OBJ, timing);
        return success("获取基础信息成功",myParams);
    }
    @Override
    public Result save(MyParams myParams) {
        ZyglDssz timing = myParams.yobj(ZyglDssz.class);
        try {
            TimingUtil.saveTimingToKettle(new VJob(timing.getZyk(),timing
                    .getIdJob()),timing);
            return success("保存成功");
        } catch (KettleException e) {
            return failed("报错定时信息失败："+e.getMessage(),e);
        }
    }
}
