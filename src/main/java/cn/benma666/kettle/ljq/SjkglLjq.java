/**
* Project Name:myservice
* Date:2018年12月16日
* Copyright (c) 2018, jingma All Rights Reserved.
*/

package cn.benma666.kettle.ljq;

import cn.benma666.iframe.MyParams;
import cn.benma666.iframe.Result;
import org.pentaho.di.core.encryption.Encr;

/**
 * 作业拦截器 <br/>
 * date: 2018年12月16日 <br/>
 * @author jingma
 * @version 0.1
 */
public class SjkglLjq extends KettleLjq {
    /**
     * 密码解密
     */
    public Result jmmm(MyParams myParams) {
        try {
            return success("密码解密成功:"+Encr.decryptPasswordOptionallyEncrypted(myParams.getString("$.yobj.password") ));
        } catch (Exception e) {
            log.error("密码解密失败:"+getSjdx(), e);
            return failed("密码解密失败，请查看系统日志分析原因:"+e.getMessage());
        }
    }
}
