/**
* Project Name:myservice
* Date:2018年12月16日
* Copyright (c) 2018, jingma All Rights Reserved.
*/

package cn.benma666.kettle.ljq;

import cn.benma666.dict.LjqType;
import cn.benma666.iframe.MyParams;
import cn.benma666.iframe.Result;
import cn.benma666.sjsj.web.LjqManager;
import org.beetl.sql.core.SqlId;

/**
 * 作业拦截器 <br/>
 * date: 2018年12月16日 <br/>
 * @author jingma
 * @version 0.1
 */
public class JcrzLjq extends KettleLjq {
    public Result getSql(MyParams myParams) {
        if("rzxz".equals(myParams.sys().getCllx())){
            return success("获取sql成功",new String[]{getSjdx().getDxzt(), getDb().getSourceSql(
                    SqlId.of("kee","getJcrzwj"),myParams)});
        }
        myParams.sys().setLjqType(LjqType.dzmrlqj);
        return success("获取sql成功",LjqManager.getSql(myParams));
    }
}
