/**
* Project Name:myservice
* Date:2018年12月16日
* Copyright (c) 2018, jingma All Rights Reserved.
*/

package cn.benma666.kettle.ljq;

import cn.benma666.iframe.MyParams;
import cn.benma666.iframe.Result;
import cn.benma666.kettle.domain.KettleKzZycs;
import cn.benma666.kettle.domain.VZycs;
import cn.benma666.sjzt.MyLambdaQuery;
import com.alibaba.fastjson.util.TypeUtils;

/**
 * 参数设置拦截器 <br/>
 * date: 2018年12月16日 <br/>
 * @author jingma
 * @version 0.1
 */
public class JobCsszLjq extends KettleLjq {

    @Override
    public Result plbc(MyParams myParams) {
        myParams.sys().setBatch(false);
        Result r = super.plbc(myParams);
        if(r.isStatus()){
            return success("参数保存成功");
        }else {
            return r;
        }
    }

    @Override
    public Result save(MyParams myParams) {
        KettleKzZycs yobj = myParams.yobj(KettleKzZycs.class);
        VZycs obj = getDb().lambdaQuery(VZycs.class).andEq(VZycs::getId, yobj.getId()).singleSimple();
        final MyLambdaQuery<KettleKzZycs> zycsMyLambdaQuery = getDb().lambdaQuery(KettleKzZycs.class,myParams.user());
        if(TypeUtils.castToBoolean(obj.getYxx())){
            //更新
            zycsMyLambdaQuery.andEq(KettleKzZycs::getIdJob, obj.getIdJob())
                    .andEq(KettleKzZycs::getKey, obj.getKey())
                    .updateSelective(KettleKzZycs.builder().value(yobj.getValue()).build());
        }else{
            //新增
            zycsMyLambdaQuery.insertSelective(yobj);
        }
        return success("保存成功");
    }

}
