package cn.benma666.kettle.ljq;

import cn.benma666.dict.Cllx;
import cn.benma666.iframe.MyParams;
import cn.benma666.iframe.Result;
import cn.benma666.sjsj.web.DefaultLjq;

public class KettleLjq extends DefaultLjq {
    @Override
    public Result jcxx(MyParams myParams) {
        if (!Cllx.dxjcxx.name().equals(getCllx(myParams)) || myParams.containsKey("$.yobj.zyk")) {
            getSjdx().setDxzt(requireNonNull(myParams.getString("$.yobj.zyk"), "资源库不能为空"));
        }
        return super.jcxx(myParams);
    }
}
