package cn.benma666.kettle.vo;

import cn.benma666.domain.BasicBean;
import lombok.*;

import javax.persistence.Id;

/**
 * 作业管理-定时设置
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ZyglDssz extends BasicBean {
    /**
     * id
     */
    @Id
    private Integer idJob;
    /**
     * 重复
     */
    private String repeat;
    /**
     * 初始执行
     */
    private String initStart;
    /**
     * 类型
     */
    private String schedulerType;
    /**
     * 秒间隔
     */
    private Integer intervalSeconds;
    /**
     * 分间隔
     */
    private Integer intervalMinutes;
    /**
     * 每天（时）
     */
    private Integer hour;
    /**
     * 每天（分）
     */
    private Integer minutes;
    /**
     * 周间隔
     */
    private String weekDay;
    /**
     * 月间隔
     */
    private Integer dayOfMonth;
    /**
     * cron
     */
    private String cron;
    /**
     * 资源库
     */
    private String zyk;
    private String start;
    private String dummy;
}
