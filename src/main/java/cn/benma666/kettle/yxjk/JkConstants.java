package cn.benma666.kettle.yxjk;

public class JkConstants {
	/*** 任务等级-1*/
	public final static String RWDJ_1="1";
	/*** 任务等级-2*/
	public final static String RWDJ_2="2";
	/*** 任务等级-3*/
	public final static String RWDJ_3="3";
	/*** 任务等级-4*/
	public final static String RWDJ_4="4";

	/*** 监控任务-任务状态：0-未知*/
	public final static String RWZT_WZ = "0";
	/*** 监控任务-任务状态：1-正常*/
	public final static String RWZT_ZC = "1";
	/*** 监控任务-任务状态：2-本身异常*/
	public final static String RWZT_BSYC = "2";
	/*** 监控任务-任务状态：3-依赖异常*/
	public final static String RWZT_YLYC = "3";
	/*** 监控任务-任务状态：4-已知异常*/
	public final static String RWZT_YZYC = "4";

	/*** 消息-消息类型：1-异常消息*/
	public final static String XXLX_YC = "1";
	/*** 消息-消息类型：2-恢复消息*/
	public final static String XXLX_HF = "2";
	/*** 消息-消息类型：3-进度反馈*/
	public final static String XXLX_FK = "3";


	/*** 异常类型-作业监控作业运行异常*/
	public final static String YCLX_001="001";
	/*** 异常类型-作业监控作业超时未运行*/
	public final static String YCLX_002="002";
	/*** 异常类型-作业监控作业日志更新超时*/
	public final static String YCLX_003="003";
	/*** 异常类型-作业监控源头数据超时*/
	public final static String YCLX_004="004";
	/*** 异常类型-作业监控数据对象不存在*/
	public final static String YCLX_005="005";
	/*** 异常类型-连接调度数据库异常*/
	public final static String YCLX_006="006";
	/*** 异常类型-具体任务不存在*/
	public final static String YCLX_007="007";
	/*** 异常类型-查询具体任务出错*/
	public final static String YCLX_008="008";
	/*** 异常类型-应用监控异常*/
	public final static String YCLX_009="009";
	/*** 异常类型-服务器监控异常*/
	public final static String YCLX_010="010";
	/*** 异常类型-服务器监控ip未配置*/
	public final static String YCLX_011="011";
	/*** 异常类型-数据库监控加载驱动失败*/
	public final static String YCLX_012="012";
	/*** 异常类型-数据库监控异常*/
	public final static String YCLX_013="013";
	/*** 异常类型-数据库监控关闭数据库失败*/
	public final static String YCLX_014="014";
	/*** 异常类型-ftp连接异常*/
	public final static String YCLX_015="015";
	/*** 异常类型-自定义sql监控sql未配置*/
	public final static String YCLX_016="016";
	/*** 异常类型-自定义sql监控执行sql无结果返回*/
	public final static String YCLX_017="017";
	/*** 异常类型-自定义sql监控无code或者msg字段返回*/
	public final static String YCLX_018="018";
	/*** 异常类型-自定义sql监控异常*/
	public final static String YCLX_019="019";

}
