package cn.benma666.kettle.yxjk;

public class JkResult {
	/**
	 * true：正常，false-异常
	 */
	private boolean flag;
	/**
	 * 监控消息
	 */
	private String msg;
	/**
	 * 任务等级
	 */
	private String rwdj;
	/**
	 * 异常类型
	 */
	private String yclx;
	/**
	 * 附加对象
	 */
	private Object data;

	/**
	 * 正常
	 * @return
	 */
	public static JkResult success(){
		JkResult result = new JkResult();
		result.setFlag(true);
		return result;
	}

	/**
	 * 正常
	 * @param msg
	 * @param rwdj
	 * @return
	 */
	public static JkResult success(String msg, String rwdj){
		JkResult result = new JkResult(true, msg, rwdj);
		return result;
	}
	/**
	 * 异常
	 * @param msg
	 * @param rwdj
	 * @return
	 */
	public static JkResult error(String msg, String rwdj){
		JkResult result = new JkResult(false, msg, rwdj);
		return result;
	}
	/**
	 * 正常
	 * @param msg
	 * @param rwdj
	 * @return
	 */
	public static JkResult success(String msg, String rwdj, String yclx){
		JkResult result = new JkResult(true, msg, rwdj, yclx);
		return result;
	}
	/**
	 * 异常
	 * @param msg
	 * @param rwdj
	 * @return
	 */
	public static JkResult error(String msg, String rwdj, String yclx){
		JkResult result = new JkResult(false, msg, rwdj, yclx);
		return result;
	}

	public JkResult() {
		super();
	}

	public JkResult(boolean flag, String msg, String rwdj) {
		super();
		this.flag = flag;
		this.msg = msg;
		this.rwdj = rwdj;
	}

	public JkResult(boolean flag, String msg, String rwdj, String yclx) {
		super();
		this.flag = flag;
		this.msg = msg;
		this.rwdj = rwdj;
		this.yclx = yclx;
	}

	public JkResult(boolean flag, String msg, String rwdj, String yclx, Object data) {
		super();
		this.flag = flag;
		this.msg = msg;
		this.rwdj = rwdj;
		this.yclx = yclx;
		this.data = data;
	}

	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getRwdj() {
		return rwdj;
	}
	public void setRwdj(String rwdj) {
		this.rwdj = rwdj;
	}
	public String getYclx() {
		return yclx;
	}
	public void setYclx(String yclx) {
		this.yclx = yclx;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
}
