package cn.benma666.kettle.yxjk;

import com.alibaba.fastjson.JSONObject;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.variables.VariableSpace;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.steps.easyexpand.EasyExpandRunBase;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Jkrwhf extends EasyExpandRunBase{
	/**
	 * 运行完成的作业<监控任务id，监控任务对象>
	 */
	public static Map<String,JSONObject> hfMap = new ConcurrentHashMap<String,JSONObject>();
	/**
	 * 处理具体每一行数据 <br/>
	 * @author jingma
	 * @param outputRow
	 */
	 protected void disposeRow(Object[] outputRow) throws Exception{
		 String jkrwid = outputRow[getFieldIndex("id")].toString();
		 JSONObject json = new JSONObject();
		 json.put("rwmc", outputRow[getFieldIndex("rwmc")].toString());
		 json.put("rwzt", outputRow[getFieldIndex("rwzt")].toString());
		 hfMap.put(jkrwid, json);
		 Jkrwzs.judgeZszt(jkrwid,json);
	 }


	@Override
	public void getFields(RowMetaInterface r, String origin, RowMetaInterface[] info, StepMeta nextStep,
			VariableSpace space) {
	}
}
