package cn.benma666.kettle.job;

import cn.benma666.iframe.InterfaceLog;
import org.pentaho.di.job.entries.easyexpand.JobEntryEasyExpandRunBase;


/**
 * 抽象JOB父类,这里存在一个日志的问题,这种任务不能很好的在页面监控
 */
public abstract class AbsJob extends JobEntryEasyExpandRunBase implements InterfaceLog {

	@Override
	protected boolean run() throws Exception {
        String name = this.getClass().getName();
        debug("Job Start:" + name);
        // 业务处理
        process();
        debug("Job End:" + name);
        return true;
	}

    /**
	 * 业务处理
	 */
	protected abstract void process() throws Exception;

    //日志封装
    public void debug(String msg, Object... appendLogArguments){
        if(jeku!=null){
            jeku.logDebug(msg,appendLogArguments);
        }else{
            log.debug(msg,appendLogArguments);
        }
    }
    public void info(String msg, Object... appendLogArguments){
        if(jeku!=null){
            jeku.logBasic(msg,appendLogArguments);
        }else{
            log.info(msg,appendLogArguments);
        }
    }
    public void error(String msg, Object... appendLogArguments){
        if(jeku!=null){
            jeku.logError(msg,appendLogArguments);
        }else{
            log.error(msg,appendLogArguments);
        }
    }

    public void debug(String msg,Throwable t){
        if(jeku!=null){
            jeku.logDebug(msg, t);
        }else{
            log.debug(msg, t);
        }
    }
    public void info(String msg,Throwable t){
        if(jeku!=null){
            jeku.logBasic(msg, t);
        }else{
            log.info(msg, t);
        }
    }
    public void error(String msg,Throwable t){
        if(jeku!=null){
            jeku.logError(msg, t);
        }else{
            log.error(msg, t);
        }
    }

    public boolean isStopped() {
        return jeku.getParentJob().isStopped();
    }
}
