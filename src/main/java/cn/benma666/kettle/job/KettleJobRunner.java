/**
* Project Name:KettleUtil
* Date:2016年6月28日
* Copyright (c) 2016, jingma All Rights Reserved.
*/

package cn.benma666.kettle.job;

import cn.benma666.kettle.domain.VJob;
import cn.benma666.kettle.mytuils.KettleManager;
import cn.benma666.sjzt.Db;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.beetl.sql.core.SqlId;

import java.util.List;

/**
 * kettle作业运行器 <br/>
 * 支持配置kettle作业一个或多个<br/>
 * date: 2016年6月28日 <br/>
 * @author jingma
 * @version 0.1
 */
public class KettleJobRunner extends AbsJob {

    ///////////配置参数//////////////////////
    private static final String JOBID_LIST = "作业id列表";
    /**
     * Creates a new instance of GenerateDataBill.
     */
    public KettleJobRunner() {
    }

    @Override
    protected void process() {
        String zyk = jeku.getRepository().getName();
        //查询当前任务所在资源库的作业
        List<JSONObject> list = Db.use(zyk).find(SqlId.of("kee", "selectJob"),
                Db.buildKeyMap("ids", configInfo.getJSONArray(JOBID_LIST)));
        //当前就是依次运行，将来根据需要可以考虑其他运行方式
        for(JSONObject jobJson:list){
            VJob vjob = jobJson.toJavaObject(VJob.class);
            try {
                //此处存在一个作业被多处调用的可能，下一层控制了同一个作业同时只能运行一个，可能造成混乱
                //所以建议一个作业不要多次使用，只能暂时只能靠自觉，瞎搞自己该遭
                info("开始执行作业："+vjob.getName());
                KettleManager.startJob(vjob);
                KettleManager.JobMap.get(vjob.getKey()).getJob().join();
                info("结束执行作业："+vjob.getName());
            } catch (Exception e) {
                error("作业执行失败："+vjob.getName(), e);
                break;
            }
        }
    }

    public String getDefaultConfigInfo() throws Exception {
        JSONObject params = new JSONObject();
        params.put(JOBID_LIST, new JSONArray());
        return JSON.toJSONString(params, true);
    }
}
