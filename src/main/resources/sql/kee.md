selectJob
===
* 查询kettle作业
```sql
select * from v_job t
-- @where(){
    -- @if(isNotEmpty(statusArr)){
    and (t.ddjd = #{ddjd} or t.ddjd is null)
    and t.yxzt in (#{join(statusArr)})
    -- @}else if(isNotEmpty(ids)){
    and t.id_job in (#{join(ids)})
    -- @}else if(isNotEmpty(id_job)){
    and t.id_job = #{id_job}
    -- @}else{
    1=2
    -- @}
-- @}
```
initUpdateJob
===
* 初始化-更新作为状态为等待
```sql
update kettle_kz_zykz t
set t.yxzt='Waiting',t.ddjd = #{ddjd},t.zhgxsj=#{globalUse("util.expDate14")}
where t.yxzt in (#{join(statusArr)}) and (t.ddjd = #{ddjd} or t.ddjd is null)
```
getJcrzwj
===
* 获取基础日志文件
```sql
select #{globalUse('util.expConcatWs',{"list":["name","'-'","gxsj","'.txt'"]\})} wjm,
'txt' wjlx,'2' xzms,'bdwjjl' sjxs,rzwj data
from kettle_kz_log jl where jl.id=#{yobj.id} and rzwj is not null
```
getYjrzwj
===
* 获取预警日志文件
```sql
select #{globalUse('util.expConcatWs',{"list":["name","'-'","gxsj","'.txt'"]\})} wjm,
'txt' wjlx,'2' xzms,'bdwjjl' sjxs,rzwj data
from kettle_kz_zyyj jl where jl.id=#{yobj.id} and rzwj is not null
```
insertLog
===
* 插入作业日志SQL
```sql
insert into kettle_kz_log(id,id_job,name,kssj,rzwj,ddjd) values(#{p1},#{p2},#{p3},#{p4},#{p5},#{p6})
```
updateLog
===
* 更新作业日志SQL
```sql
update kettle_kz_log l set l.jssj=#{p1},l.yxjg=#{p2} where l.id=#{p3}
```
insertZyyj
===
* 插入作业预警
```sql
insert into kettle_kz_zyyj(id_job,name,rzwj,xx, rzjb, sfcw, sz,rztd,ddjd)
values(#{p1},#{p2},#{p3},#{p4},#{p5},#{p6},#{p7},#{p8},#{p9})
```